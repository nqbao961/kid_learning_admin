var config = {
    apiKey: "AIzaSyCX1yFkfD6lULqjOfG2bSEsdRhLlfECyBg",
    authDomain: "kid-learning-bk.firebaseapp.com",
    databaseURL: "https://kid-learning-bk.firebaseio.com",
    projectId: "kid-learning-bk",
    storageBucket: "kid-learning-bk.appspot.com",
    messagingSenderId: "239642608705"
};

firebase.initializeApp(config);
var functions = firebase.functions();
var db = firebase.firestore();

function getAllTopics() {
    db.collection("topics").get().then(function (querySnapshot) {
        querySnapshot.forEach(function (doc) {
            $("#topics-collection-list").append('<a href="javascript:void(0)" class="list-group-item topic-item" data-id="' + doc.id + '">' + doc.id + '</a>');
            $("#slide_topic, #quiz_topic, #lesson_topic").append('<option value="' + doc.id + '">' + doc.data().title.en + '</option>');
        });
        $('.topic-item').on("click", function () {
            $('.topic-item').removeClass("active");
            $(this).addClass("active");
            getTopicDocument($(this));
        });
    });
}

function getTopicDocument($this) {
    db.collection("topics").doc($this.data('id')).get().then(function (doc) {
        $('#topic_title_en').val(doc.data().title.en);
        $('#topic_title_vi').val(doc.data().title.vi);
        $('.topic-levels').each(function () {
            if (doc.data().levels.includes($(this).val())) {
                $(this).prop("checked", true);
            }
            else {
                $(this).prop("checked", false);
            }
        });
        $('#topic_thumbnail').val(doc.data().thumbnail);
        $('#topic_thumbnail_preview').attr("src", doc.data().thumbnail);
    });
}

function getAllSlides() {
    db.collection("slides").get().then(function (querySnapshot) {
        querySnapshot.forEach(function (doc) {
            $("#slides-collection-list").append('<a href="javascript:void(0)" class="list-group-item slide-item" data-id="' + doc.id + '">' + doc.id + '</a>');
            $('#lesson_slides').append('<option value="' + doc.id + '">' + doc.id + '</option>');
        });
        $('.slide-item').on("click", function () {
            $('.slide-item').removeClass("active");
            $(this).addClass("active");
            getSlideDocument($(this));
        });
    });
}

function getSlideDocument($this) {
    db.collection("slides").doc($this.data('id')).get().then(function (doc) {
        $('#slide_content_en').val(doc.data().content.en);
        $('#slide_content_vi').val(doc.data().content.vi);
        $('#slide_content_pronounce').val(doc.data().content.pronounce);
        $('#slide_level option[value=' + doc.data().level + ']').prop('selected', true);
        $('#slide_image').val(doc.data().image);
        $('#slide_image_preview').attr("src", doc.data().image);
        $('#slide_topic option[value=' + doc.data().topic_id + ']').prop('selected', true);
    });
}

function objectifyForm(formArray) {
    var returnArray = {};
    for (var i = 0; i < formArray.length; i++) {
        returnArray[formArray[i]['name']] = formArray[i]['value'];
    }
    return returnArray;
}

$.fn.serializeObject = function () {
    var o = {};
    var a = this.serializeArray();
    var k = [];
    $.each(a, function () {
        var hasBrackets = this.name.match(/\[(.*?)\]/);
        if (hasBrackets) {
            var inputName = this.name.replace(hasBrackets[0], '');
            if (o[inputName] == undefined) {
                o[inputName] = {};
                k[inputName] = 0;
            } else {
                k[inputName]++;
            }
            var inputNameKey = hasBrackets[1] || k[inputName];
            o[inputName][inputNameKey] = this.value || '';
        } else {
            o[this.name] = this.value || '';
        }

    });
    return o;
};

function addSlide($data) {
    var dataObj = objectifyForm($data);

    db.collection("slides").where('level', '==', dataObj.slide_level).where('topic_id', '==', dataObj.slide_topic).get().then(function (querySnapshot) {
        var slideNo = querySnapshot.size + 1;
        db.collection("slides").doc('slide_' + dataObj.slide_level + '_' + dataObj.slide_topic + '_' + slideNo).set({
            content: {
                en: dataObj.slide_content_en,
                vi: dataObj.slide_content_vi,
                pronounce: dataObj.slide_content_pronounce
            },
            image: dataObj.slide_image,
            level: dataObj.slide_level,
            topic_id: dataObj.slide_topic,
            topic_ref: db.doc('/topics/' + dataObj.slide_topic)
        }).then(function () {
            alert('Added slide successfully!');
        });
    })
}

function deleteSlide() {
    if (confirm("Delete this slide?")) {
        db.collection("slides").doc($('.slide-item.active').data('id')).delete().then(function () {
            console.log("Slide successfully deleted!");
        }).catch(function (error) {
            console.error("Error removing document: ", error);
        });
    }
}

function getAllQuizzes() {
    db.collection("quizzes").get().then(function (querySnapshot) {
        querySnapshot.forEach(function (doc) {
            $("#quizzes-collection-list").append('<a href="javascript:void(0)" class="list-group-item quiz-item" data-id="' + doc.id + '">' + doc.id + '</a>')
        });
        $('.quiz-item').on("click", function () {
            $('.quiz-item').removeClass("active");
            $(this).addClass("active");
            getQuizDocument($(this));
        });
    });
}

function getQuizDocument($this) {
    db.collection("quizzes").doc($this.data('id')).get().then(function (doc) {
        console.log(doc.data());
        // $('#quiz_question_content').val(doc.data().question.content);
        // $('#quiz_question_image').val(doc.data().question.image);
        // $('#slide_content_pronounce').val(doc.data().content.pronounce);
        // $('#slide_level option[value=' + doc.data().level + ']').prop('selected', true);
        // $('#slide_image').val(doc.data().image);
        // $('#slide_image_preview').attr("src", doc.data().image);
        // doc.data().topic.get().then(function (topicDoc) {
        //     $('#slide_topic option[value=' + topicDoc.id + ']').prop('selected', true);
        // })
    });
}

function addQuiz($data) {
    var dataObj = objectifyForm($data);

    db.collection("quizzes").where('level', '==', dataObj.quiz_level).where('topic_id', '==', dataObj.quiz_topic).get().then(function (querySnapshot) {
        var quizNo = querySnapshot.size + 1;
        db.collection("quizzes").doc('quiz_' + dataObj.quiz_level + '_' + dataObj.quiz_topic + '_' + quizNo).set({
            type: dataObj.quiz_type,
            question: {
                content: dataObj.quiz_question_content,
                image: dataObj.quiz_question_image,
                pronounce: dataObj.quiz_question_pronounce
            },
            choices: [
                dataObj.quiz_choice_A,
                dataObj.quiz_choice_B,
                dataObj.quiz_choice_C,
                dataObj.quiz_choice_D
            ],
            answer_multiple_choice: parseInt(dataObj.quiz_answer_multiple_choice),
            answer_fill_blank: dataObj.quiz_answer_fill_blank,
            level: dataObj.quiz_level,
            topic_id: dataObj.quiz_topic,
            topic_ref: db.doc('/topics/' + dataObj.quiz_topic),
            total_result: {
                correct_num: 0,
                incorrect_num: 0
            },
            results: {}
        }).then(function () {
            alert('Added quiz successfully!');
        });
    })
}

function deleteQuiz() {
    if (confirm("Delete this quiz?")) {
        db.collection("quizzes").doc($('.quiz-item.active').data('id')).delete().then(function () {
            console.log("Quiz successfully deleted!");
        }).catch(function (error) {
            console.error("Error removing document: ", error);
        });
    }
}

function getAllLessons() {
    db.collection("lessons").get().then(function (querySnapshot) {
        querySnapshot.forEach(function (doc) {
            $("#lessons-collection-list").append('<a href="javascript:void(0)" class="list-group-item lesson-item" data-id="' + doc.id + '">' + doc.id + '</a>');
        });
        $('.lesson-item').on("click", function () {
            $('.lesson-item').removeClass("active");
            $(this).addClass("active");
            //getLessonDocument($(this));
        });
    });
}

function addLesson(dataObj) {
    var lessonSlideRefs = [];
    Object.values(dataObj.lesson_slides).forEach(function (slide) {
        lessonSlideRefs.push(db.doc('/slides/' + slide));
    })
    db.collection("lessons").where('level', '==', dataObj.lesson_level).where('topic_id', '==', dataObj.lesson_topic).get().then(function (querySnapshot) {
        var lessonNo = querySnapshot.size + 1;
        db.collection("lessons").doc("lesson_" + dataObj.lesson_level + "_" + dataObj.lesson_topic + "_" + lessonNo).set({
            no: lessonNo,
            slides: lessonSlideRefs,
            level: dataObj.lesson_level,
            topic_id: dataObj.lesson_topic,
            topic_ref: db.doc('/topics/' + dataObj.lesson_topic)
        })
    })
}
function getRandom(arr, n) {
    var result = new Array(n),
        len = arr.length,
        taken = new Array(len);
    if (n > len)
        throw new RangeError("getRandom: more elements taken than available");
    while (n--) {
        var x = Math.floor(Math.random() * len);
        result[n] = arr[x in taken ? taken[x] : x];
        taken[x] = --len in taken ? taken[len] : len;
    }
    return result;
}

$(document).ready(function () {
    var testFunction = functions.httpsCallable('getQuizList');
    testFunction({level: 'easy', topic: 'animals'}).then(function (result) {
        console.log(result.data);
    });
    db.collection('quizzes')
    .where('level', '==', 'easy')
    .where('topic_id', '==', 'animals')
    .get().then(querySnapshot => {
        var result = [];
            var docs = getRandom(querySnapshot.docs, 3);
            docs.forEach((doc)=>{
                result.push(db.doc(doc.ref.path));
            })
            console.log(result);
    })
    // db.collection('lessons')
    // .doc('lesson_easy_animals_1')
    // .get().then(doc => {
    //     console.log(doc.data().slides);
    // })

    getAllTopics();
    getAllSlides();
    getAllLessons();
    getAllQuizzes();

    $('.btn-reset-form').on('click', function () {
        $(this).closest('form')[0].reset();
        $('.img-preview').attr('src', '');
    });

    $('#add_slide_btn').on('click', function () {
        addSlide($(this).closest('form').serializeArray());
    })

    $('#delete_slide_btn').on('click', function () {
        deleteSlide();
    })

    $('.quiz-answer-fill-blank').hide();

    $('#add_quiz_btn').on('click', function () {
        addQuiz($(this).closest('form').serializeArray());
    })

    $('#delete_quiz_btn').on('click', function () {
        deleteQuiz();
    })

    $('select#quiz_type').on('change', function (e) {
        var optionSelected = $("option:selected", this);
        var valueSelected = this.value;
        if (valueSelected == 'multiple-choice') {
            $('.quiz-choice-container').show();
            $('.quiz-answer-multiple-choice').show();
            $('.quiz-answer-fill-blank').hide();
        }
        else if (valueSelected == 'fill-blank') {
            $('.quiz-choice-container').hide();
            $('.quiz-answer-multiple-choice').hide();
            $('.quiz-answer-fill-blank').show();
        }
    });

    $('#add_lesson_btn').on('click', function () {
        addLesson($(this).closest('form').serializeObject());
    })
});